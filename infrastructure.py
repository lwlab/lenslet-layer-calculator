# Lenslet layer calculator
# by Colin Merkel

import numpy, cmath, json

# The configuration for the system, which comes
# from a JSON file in the config directory.
json_data = open("./config/config.json")
config = json.load(json_data)
json_data.close()

# The GenericDataSource object is an object which
# simply stores a static data value. When sampled,
# it always returns the same value.
class GenericDataSource(object):
	def __init__(self, value):
		# Default: set the value to 5.
		self.value = value

	def sample(self):
		return self.value

	def average(self):
		return self.value

	def scramble(self):
		pass

	def reset(self):
		pass

class GenericFrequencyDependentDataSource(object):
	def sample(self, frequency):
		return self.value

	def average(self, frequency):
		return self.value

# The GaussianDataSource object represents a data source
# with a mean and standard deviation which is distributed
# in a gaussian way.
class GaussianDataSource(GenericDataSource):
	def __init__(self, mean, standard_deviation):
		self.mean = mean
		self.standard_deviation = standard_deviation

	# This function returns a sample from the distribution. Numpy's randn function
	# returns a Gaussian sample of mean 0 and std.dev 1.
	def sample(self):
		return self.standard_deviation * numpy.random.randn() + self.mean

	def average(self):
		return self.mean

# The GaussianFixedDataSource is like a gaussian source, but it stays fixed during
# a particular calculation. It can be "scrambled" at another time to choose another
# possible value, but then remains fixed until another scramble is issued.
class GaussianFixedDataSource(object):
	def __init__(self, mean, standard_deviation):
		self.mean = mean
		self.standard_deviation = standard_deviation
		self.this_sample = mean

	def sample(self):
		return self.this_sample

	def average(self):
		return self.this_sample

	def scramble(self):
		self.this_sample = self.standard_deviation * numpy.random.randn() + self.mean

	def reset(self):
		self.this_sample = self.mean

class Wave(object):
	def __init__(self):
		self.frequency 		= 150e3 # 150 GHz
		self.path_length 	= 0

		# The magnitude of the electric field,
		# including imaginary components.
		self.electric_field = 0

	# This function returns a copy of the wave.
	def duplicate(self):
		w = Wave()
		w.frequency = self.frequency
		w.electric_field = self.electric_field
		w.path_length = self.path_length
		return w

# The Layer object reprsents an individual layer.
class Layer(object):
	def __init__(self):
		self.name 	   = "Undefined layer"

		# Material properties of the layer:
		self.thickness		= GaussianDataSource(5,0)
		self.permittivity	= GaussianDataSource(1,0)
		self.resistivity	= GenericDataSource(1e10)
		self.losstangent	= None

		# This considers whether we care about internal
		# reflections. In some (thick) materials, if this
		# is "True", it will behave as an etalon and cause
		# crazy results. So for thick things make it "False"
		self.consider_internal_reflections = True

		# The layers left and right of this layer:
		self.left	   = None
		self.right	   = None

	def scramble(self):
		self.thickness.scramble()
		self.permittivity.scramble()
		self.resistivity.scramble()

	def reset(self):
		self.thickness.reset()
		self.permittivity.reset()
		self.resistivity.reset()

	def __repr__(self):
		return "%s [Layer]" % self.name

	def index(self):
		return numpy.sqrt( self.permittivity.sample() )

	def calibrate(self, frequency):
		# If the layer has a defined loss tangent, we'll use that
		# instead of the resistivity value.
		if self.losstangent != None:
			self.resistivity = GenericDataSource(  1/( 2 * 3.141592 * frequency * self.permittivity.average() * 8.85e-12 * self.losstangent.average() ) )

	def visualize(self):
		return (" [ %s ] -> " % self.name) + self.right.visualize()

	# This function returns the absolute (not relative) complex
	# permittivity, including resistive contributions.
	def complex_permittivity(self, wave):
		return (self.permittivity.sample()* 8.85e-12) + 1j / (self.resistivity.sample() * wave.frequency * 2 * 3.14159)

	# This function returns the complex propagation factor, which
	# is multiplied by the wavevector when propagating across the
	# material. It depends on the wavelength and the index of refraction, 
	# as well as the thickness. It performs sampling to get these numbers,
	# so each time it is called it will return a sampled value of the propagation
	# factor.
	def propagate_wave(self, wave):
		# k = w * index / c
		w = self.thickness.sample()
		k = wave.frequency * 2 * 3.14159 * numpy.sqrt( 1.256e-6 * self.complex_permittivity(wave) )
		wave.electric_field *= numpy.exp( 1j * k *  w)
		wave.path_length 	+= w

	# The receiveRight and receiveLeft functions receive
	# waves from the left and right sides. When a layer
	# receives a wave, it propagates it to the other side, 
	# then decides how it will reflect on the interface.
	def receive(self, wave, side):
		# First let's forget about the wave if it has a very small
		# magnitude. 
		if abs(wave.electric_field) < config['e_field_threshold']:
			return

		# Now, propagate the wave from the right side 
		# to the left side.
		self.propagate_wave(wave)

		# Now we need to use the other material's parameters
		# to decide what we want to do.
		if side == "right":
			other_material 	= self.left
		elif side == "left":
			other_material 	= self.right
		else: 
			raise Exception("There's a problem: wave came in from nowhere!")
		
		n2 = other_material.index()
		n1 = self.index()

		reflected_wave 		= wave.duplicate()
		transmitted_wave 	= wave.duplicate()

		reflected_wave.electric_field 	*= (n1 - n2) / (n1 + n2)
		transmitted_wave.electric_field *= 2*(n1) / (n1 + n2)

		# Finally, we'll process the reflected and transmitted waves. The reflected wave
		# is picked up by this material. The transmitted wave is sent off to the other material.
		# This part is recursive. The only terminating condition is that the waves are either absorbed
		# or reach small enough amplitude to be ignored (see start of this function).
		if side == "left":
			self.receiveRight(reflected_wave)
			other_material.receiveLeft(transmitted_wave)
		elif side == "right":
			self.receiveLeft(reflected_wave)
			other_material.receiveRight(transmitted_wave)

		# All done.
		return

	def receiveRight(self, wave):
		return self.receive(wave, "right")

	def receiveLeft(self, wave):
		return self.receive(wave, "left")

# This object represents a layer which calculates all of the fields
# incident into it, and does not participate in reflections. Termination
# layers are the end caps of everything.
class SourceLayer(Layer):
	def __init__(self):
		Layer.__init__(self)
		self.clear()
		self.waves = []

	# This function is called when we want to get rid of any residues
	# left from previous calculations.
	def clear(self):
		self.surface_electric_field = 0
		self.waves = []

	# This function returns the power associated with a particular electric
	# field. 
	def power(self, electric_field):
		return (self.index()) * abs(electric_field)**2

	def absorbed_power(self):
		# We need to consider the coherence length in this calculation. The
		# procedure (to first order) is to sum linearly all contributions within
		# the coherence length. Then, add all of the resulting fields together in
		# the power domain at the end.
		power = 0
		calculated_list = []
		for j in range(len(self.waves)):
			if j in calculated_list:
				continue
			# First, find all groups of waves which have similar path-lengths.
			group = [j]
			for i in range(len(self.waves)):
				if i == j or i in calculated_list:
					continue
				elif abs(self.waves[j].path_length - self.waves[i].path_length) < config['coherence_length']:
					# Add wave i to the list of nearby waves.
					group.append(i)
			# Now we need to add all of the waves in the group linearly.
			field = 0
			for i in group:
				field += self.waves[i].electric_field
			power += self.power(field)
			# Now we have to remove all of the elements in group from being
			# double-counted.
			for i in group:
				calculated_list.append(i)

		return power


	def visualize(self):
		return (" [ %s (source layer) ] -> " % self.name) + self.right.visualize()

	def receiveRight(self, wave):
		self.waves.append(wave)

# This the right-most element, which is fully absorptive. Like
# a source layer, it doesn't participate in refletions, and it
# keeps track of all fields incident on it.
class TerminationLayer(SourceLayer):
	def visualize(self):
		return " [ %s (termination layer) ] " % self.name

	def receiveLeft(self, wave):
		self.waves.append(wave)