# Lenslet layer calculator
# by Colin Merkel

import random, math, numpy

class Parameter(object):
	def __init__(self, name):
		self.name = name
		self.value = 0.5
		self.range = [ 0, 1 ]

	# Defines addition between parameter and float
	def __add__(self, other):
		p = Parameter(self.name)
		p.range = self.range
		p.value = self.value + other
		return p

	# Return the logarithm of this parameter's bayesian prior.
	def prior(self):
		if self.value < 0: 
			return -1000
		elif self.value > 1:
			return -1000
		else:
			return 0

	def __sub__(self, other):
		return self.__add__(-other)

	def __mul__(self, other):
		p = Parameter(self.name)
		p.range = self.range
		p.value = self.value * other
		return p

	def __float__(self):
		return self.evaluate()

	def evaluate(self):
		return self.value * (self.range[1] - self.range[0]) + self.range[0]

class OptimizationController(object):
	def __init__(self):
		self.iterations = 1000
		self.learning_rate = 0.01

	# This function defines the parameter prior. It returns the logarithm of
	# the bayesian prior over the parameters.
	def parameter_prior(self, parameters):
		return sum( [ p.prior() for p in self.parameters ] )

	# This function defines the success metric of the optimizer.
	def evaluate(self, parameters):
		return sum([ p.prior() for p in parameters ]) + 0.5*self.metric([p.evaluate() for p in parameters])

	def metric(self, parameters):
		return 0

	def optimize(self):
		for i in range( self.iterations ):
			print "Iteration %d..." % i
			# Define a normalized weights vector.
			weights = [ random.random() - 0.5 for r in range( len(self.parameters) ) ]
			weights = numpy.divide( weights , math.sqrt(sum( [ w**2 for w in weights ] )))
			# Now let's find the gradient of the metric over this vector.
			left 	= self.evaluate( [ p - w*self.learning_rate for p,w in zip(self.parameters,weights) ] )
			right	= self.evaluate( [ p + w*self.learning_rate for p,w in zip(self.parameters,weights) ] )
			print "Current performance: %f " % right
			derivative = right - left
			print "Current derivative: %f " % derivative
			print "Current parameter value: %f" % self.parameters[0]
			# Perform the parameter update.
			self.parameters = [ (p + w*derivative) for p,w in zip(self.parameters, weights) ]

# This optimizer uses simulated annealing.
class SAOptimizationController(OptimizationController):
	def acceptance(self, performance, best_performance, temperature):
		if performance > best_performance:
			# Use the Kirkpatrick formulation.
			return True 
		else:
			# Or else, probabilitically accept the result.
			print "Likelihood: %f, %f, %f, %f" % (performance, best_performance, temperature, -(best_performance - performance) / temperature)
			return random.random() > math.exp( -(best_performance - performance) / temperature )

	def optimize(self):
		# Initialize the best values to the current settings.
		best_parameters  = self.parameters 
		best_performance = self.evaluate( self.parameters )
		for i in range( self.iterations ):
			T = 10*( 1 - (i+0.01)/self.iterations )
			print "Iteration T = %f: best performance: %f" % (T, best_performance)
			
			# Choose a direction for the neighbor. The step is proportional
			# to a random number, the temperature, and the learning rate, and
			# can be negative or positive.
			neighbor_step = [ T * self.learning_rate * (random.random() - 0.5) for r in range( len(self.parameters) ) ]
			neighbor = [ p - s for p,s in zip(self.parameters, neighbor_step) ]
			neighbor_performance = self.evaluate( neighbor )
			# Now we must decide whether to move to the neighbor position
			# or not.
			if self.acceptance( neighbor_performance, best_performance, T ):
				self.parameters = neighbor
				if neighbor_performance > best_performance:
					best_parameters = neighbor
					best_performance = neighbor_performance

		self.parameters = best_parameters