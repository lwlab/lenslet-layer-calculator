# Lenslet layer calculator
# by Colin Merkel

from analyzer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		atmosphere 				= SourceLayer()
		atmosphere.name 		= "Atmosphere"
		atmosphere.thickness	= GenericDataSource(0)
		atmosphere.permittivity	= GenericDataSource(1.00)

		alumina 			= Layer()
		alumina.name		= "Alumina"
		alumina.thickness 	= GenericDataSource(4e-3) # 4 mm
		alumina.permittivity= GenericDataSource(3.20**2)
		alumina.losstangent = GenericDataSource(0.9e-4)

		vacuum	 			= TerminationLayer()
		vacuum.name 		= "Vaccuum"
		vacuum.thickness	= GenericDataSource(1)
		vacuum.permittivity	= GenericDataSource(1.00)

		# Return everything together.
		self.structure 	= [atmosphere, alumina, vacuum]
		self.source 	= atmosphere
		self.terminator = vacuum

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

a.calculate( incident_field )

frequencies 	= numpy.linspace(60, 250, 400)

transmissions 	= []
reflectivities 	= []

for f in frequencies:
	incident_field.frequency = f*1e9 # f gigahertz
	a.calculate( incident_field )
	transmissions.append( (a.transmission())*100 )


plot.plot(frequencies, transmissions)

plot.ylim([0,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(frequencies), max(frequencies)])
plot.xlabel("Frequency [GHz]")
plot.show()