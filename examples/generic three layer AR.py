# Lenslet layer calculator
# by Colin Merkel

from analyzer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class SingleLayerARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GenericDataSource(0.4175e-3) # 1.6 mm (center freq: 100 GHz)
		coating.permittivity= GenericDataSource(3.2)

		alumina 			= TerminationLayer()
		alumina.name		= "Alumina"
		alumina.thickness 	= GenericDataSource(40e-3) # 4 cm
		alumina.permittivity= GenericDataSource(10)

		# Return everything together.
		self.structure 	= [vacuum, coating, alumina]

class DualLayerARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GenericDataSource(.53e-3) 
		coating.permittivity= GenericDataSource(2)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	= GenericDataSource(.335e-3) 
		coating2.permittivity= GenericDataSource(5)

		alumina 			= TerminationLayer()
		alumina.name		= "Alumina"
		alumina.thickness 	= GenericDataSource(40e-3) # 4 cm
		alumina.permittivity= GenericDataSource(10)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, alumina]

class TripleLayerARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GenericDataSource(0.53e-3) # 1.6 mm (center freq: 100 GHz)
		coating.permittivity= GenericDataSource(2)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	= GenericDataSource(0.375e-3) # 1.6 mm (center freq: 100 GHz)
		coating2.permittivity= GenericDataSource(4)

		coating3			= Layer()
		coating3.name		= "AR coating 3"
		coating3.thickness 	= GenericDataSource(0.2825e-3) # 1.6 mm (center freq: 100 GHz)
		coating3.permittivity= GenericDataSource(7)

		alumina 			= TerminationLayer()
		alumina.name		= "Alumina"
		alumina.thickness 	= GenericDataSource(40e-3) # 4 cm
		alumina.permittivity= GenericDataSource(10)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, alumina]

frequencies 	= numpy.linspace(0.01, 2, 200)


for a in [SingleLayerARController(), DualLayerARController(), TripleLayerARController()]:
	print "Preparing to analyze network:"
	transmissions  = []
	print a.visualize()
	for f in frequencies:
		incident_field.frequency = 100e9 * f
		a.calculate( incident_field )
		transmissions.append( (a.transmission())*100 )

	plot.plot(frequencies, transmissions)

plot.ylim([70,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(frequencies), max(frequencies)])
plot.xlabel("Frequency, normalized to f_0")
plot.show()