# Lenslet layer calculator
# by Colin Merkel

from analyzer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GenericDataSource(55.5e-9) # 75 nm
		coating.permittivity= GenericDataSource(1.41**2)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	= GenericDataSource(53.2e-9) # 75 nm
		coating2.permittivity= GenericDataSource(2.24**2)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, silicon]

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

a.calculate( incident_field )

wavelengths 	= numpy.linspace(300, 1100, 400)
reflectivities  = []
for w in wavelengths:
	incident_field.frequency = 3e8 / (w*1e-9)
	a.calculate( incident_field )
	reflectivities.append( (a.reflection())*100 )

plot.plot(wavelengths, reflectivities)

plot.ylim([0,25])
plot.ylabel("Reflection [%]")
plot.xlim([min(wavelengths), max(wavelengths)])
plot.xlabel("Wavelength (nm)")
plot.show()