# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity = GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GaussianFixedDataSource(0.31625e-3, 10e-6) # 75 nm
		coating.permittivity= GaussianFixedDataSource(2.20689, 0.1)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	=  GaussianFixedDataSource(0.19375e-3, 10e-6) # 75 nm
		coating2.permittivity= GaussianFixedDataSource(5.3382, 0.1)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, silicon]

class TARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(2)), 10e-6) # 75 nm
		coating.permittivity= GaussianFixedDataSource(2, 0.1)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(3.983)), 10e-6) # 75 nm
		coating2.permittivity= GaussianFixedDataSource(3.983, 0.1)

		coating3 			= Layer()
		coating3.name		= "AR coating 3"
		coating3.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(7.5)), 10e-6) # 75 nm
		coating3.permittivity= GaussianFixedDataSource(7.5, 0.1)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]

a = ARController()
b = TARController()
b.reset()
print "Preparing to analyze network: 'two-layer'"
print a.visualize()
print "'three-layer'"
print b.visualize()

frequencies = numpy.linspace(75,240,200)
toki_trans = []
colin_trans = []

for f in frequencies:
	incident_field.frequency = f*1e9
	b.structure[3].permittivity = GenericDataSource(7.5)
	b.structure[3].thickness 	= GenericDataSource(0.25*3e8/(160e9 * numpy.sqrt(7.5))) # 75 nm
	b.calculate( incident_field )
	colin_trans.append( b.reflection()*100 )
	b.structure[3].permittivity = GenericDataSource(7)
	b.structure[3].thickness 	= GenericDataSource(0.25*3e8/(160e9 * numpy.sqrt(7))) # 75 nm
	b.calculate( incident_field )
	toki_trans.append( b.reflection()*100 )

plot.plot(frequencies, colin_trans,   'r-')
plot.plot(frequencies, toki_trans, 'b-')

print "Colin: %f %% RMS" % numpy.sqrt(sum( [ t**2 for t in colin_trans ])/len(colin_trans))
print "Toki: %f %% RMS" % numpy.sqrt(sum( [ t**2 for t in toki_trans ])/len(toki_trans))

plot.ylim([90, 100])
plot.ylabel("Transmission [%]")
plot.xlim([75, 240])
plot.xlabel("Frequency [GHz]")
plot.show()