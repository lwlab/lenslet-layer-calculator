# Lenslet layer calculator
# by Colin Merkel

from analyzer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating.permittivity= GenericDataSource(1.98**2)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, silicon]

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

a.calculate( incident_field )

wavelengths 	= numpy.linspace(400, 1100, 400)
indices			= [1.98, 1.94, 1.89]
depths			= [75.13, 89.67, 118.16]

for i in range(len(indices)):
	reflectivities 	= []
	a.structure[1].thickness = GenericDataSource(depths[i]*1e-9)
	a.structure[1].permittivity = GenericDataSource(indices[i]**2)

	for w in wavelengths:
		incident_field.frequency = 3e8 / (w*1e-9) # f gigahertz
		a.calculate( incident_field )
		reflectivities.append( (a.reflection())*100 )


	plot.plot(wavelengths, reflectivities)

plot.ylim([0,100])
plot.ylabel("Reflection [%]")
plot.xlim([min(wavelengths), max(wavelengths)])
plot.xlabel("Wavelength (nm)")
plot.show()