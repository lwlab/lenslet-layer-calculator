# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating.permittivity= GenericDataSource(1.98**2)

		coating2 			= Layer()
		coating2.name		= "AR coating"
		coating2.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating2.permittivity= GenericDataSource(2.7**2)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, silicon]

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

class AROptimizer(SAOptimizationController):
	def metric(self, parameters):
		# The parameters are imported in order.
		depth1 			= parameters[0]
		depth2 			= parameters[1]
		# Set the depth of the coating layer.
		a.structure[1].thickness = GenericDataSource(depth1)
		a.structure[2].thickness = GenericDataSource(depth2)

		# Now we'll loop through the bandwidth and determine
		# the quality of the reflections.
		frequencies 	= numpy.linspace(75,240, 20)
		RMS_reflection 	= 0
		for f in frequencies:
			incident_field.frequency = f * 1e9 # f gigahertz
			a.calculate( incident_field )
			RMS_reflection += (a.reflection()*100)**2

		# The higher the metric function, the better the performance. So we'll
		# return a negative number for the RMS to minimize reflections.
		return -( RMS_reflection / len(frequencies) )


o = AROptimizer()

depth1 = Parameter("AR coating 1")
depth1.range = [ 0, 1e-3 ]

depth2 = Parameter("AR coating 2")
depth2.range = [ 0, 1e-3 ]

o.parameters = [ depth1, depth2 ]

# Now we optimize.
o.optimize()
print "Optimization complete."

# Now let's visualize the result of the optimization.
print "The best optimized depths were: %f mm, %f mm" % (o.parameters[0]*1e3, o.parameters[1]*1e3)

frequencies 	= numpy.linspace(75,240, 400)
# Set the structure thickness to the optimized thickness.

transmissions = []
control_trans = []
midway_trans  = []

for f in frequencies:
	# First, calculate for the proposed thickness.
	a.structure[1].thickness = GenericDataSource(o.parameters[0].evaluate())
	a.structure[2].thickness = GenericDataSource(o.parameters[1].evaluate())
	incident_field.frequency = f * 1e9 # f gigahertz
	a.calculate( incident_field )
	transmissions.append( a.transmission()*100 )
	# Now, calculate for the zero thickness case.
	a.structure[1].thickness = GenericDataSource(0)
	a.structure[2].thickness = GenericDataSource(0)
	a.calculate( incident_field )
	control_trans.append( a.transmission()*100 )

plot.plot(frequencies, transmissions,'r-')
plot.plot(frequencies, control_trans,'b-')

plot.ylim([0,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(frequencies), max(frequencies)])
plot.xlabel("Frequency (GHz)")
plot.show()