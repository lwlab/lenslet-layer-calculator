# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity = GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GaussianFixedDataSource(0.31625e-3, 10e-6) # 75 nm
		coating.permittivity= GaussianFixedDataSource(2.20689, 0.1)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	=  GaussianFixedDataSource(0.19375e-3, 10e-6) # 75 nm
		coating2.permittivity= GaussianFixedDataSource(5.3382, 0.1)

		coating3 			= Layer()
		coating3.name		= "AR coating"
		coating3.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating3.permittivity= GenericDataSource(5.36)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]

class TARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(2)), 10e-6) # 75 nm
		coating.permittivity= GaussianFixedDataSource(2, 0.1)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(3.983)), 10e-6) # 75 nm
		coating2.permittivity= GaussianFixedDataSource(3.983, 0.1)

		coating3 			= Layer()
		coating3.name		= "AR coating 3"
		coating3.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(7.5)), 10e-6) # 75 nm
		coating3.permittivity= GaussianFixedDataSource(7.5, 0.1)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]

a = ARController()
b = TARController()
print "Preparing to analyze network: 'two-layer'"
print a.visualize()

def metric(parameters):
	# The parameters are imported in order.
	p1			= parameters[0]
	p2			= parameters[1]
	p3			= parameters[2]
	# Set the depth of the coating layer.
	a.structure[1].thickness = GenericDataSource(3e8*0.25/(numpy.sqrt(p1) * 160e9))
	a.structure[2].thickness = GenericDataSource(3e8*0.25/(numpy.sqrt(p2) * 160e9))
	a.structure[3].thickness = GenericDataSource(3e8*0.25/(numpy.sqrt(p3) * 160e9))
	a.structure[1].permittivity = GenericDataSource(p1)
	a.structure[2].permittivity = GenericDataSource(p2)
	a.structure[3].permittivity = GenericDataSource(p3)

	# Now we'll loop through the bandwidth and determine
	# the quality of the reflections. Currently using 
	frequencies 	= numpy.union1d( numpy.union1d( numpy.linspace(75,115,5), numpy.linspace(130,170,5) ), numpy.linspace(190,240,5))
	RMS_reflection 	= 0
	for f in frequencies:
		incident_field.frequency = f * 1e9 # f gigahertz
		a.calculate( incident_field )
		RMS_reflection += (a.reflection()*100)**2

	# The higher the metric function, the better the performance. So we'll
	# return a negative number for the RMS to minimize reflections.
	return -numpy.sqrt( RMS_reflection / len(frequencies) )

# Grid-search over the space of possible thicknesses.
best = -100
bestp = [ 0, 0, 0 ]
iteration = 0

p1domain = numpy.linspace(2,4,60)
p2domain = numpy.linspace(2, 6, 80)
p3domain = numpy.linspace(6, 11, 100)

for p1 in p1domain:
	iteration += 1 
	print "Currently %0.2f%% done." % (100*iteration/len(p1domain))
	for p2 in p2domain:
		for p3 in p3domain:
			c = metric([p1, p2, p3])
			if c > best:
				best = c
				bestp = [p1, p2, p3]
				print "Found best performer: %f%% RMS @ %s" % (-c, bestp) 

print "Gridsearch complete. Best performance: %f%% RMS. \nBest parameters: %s" % (best, bestp)