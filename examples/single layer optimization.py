# Lenslet layer calculator
# by Colin Merkel

import random, math, numpy

class Parameter(object):
	def __init__(self, name):
		self.name = name
		self.value = 0.5
		self.range = [ 0, 1 ]

	# Defines addition between parameter and float
	def __add__(self, other):
		p = Parameter(self.name)
		p.range = self.range
		p.value = self.value + other
		return p

	def __sub__(self, other):
		return self.__add__(-other)

	def __mul__(self, other):
		p = Parameter(self.name)
		p.range = self.range
		p.value = self.value * other
		return p

	def __float__(self):
		return self.evaluate()

	def evaluate(self):
		return self.value * (self.range[1] - self.range[0]) + self.range[0]

class OptimizationController(object):
	def __init__(self):
		self.iterations = 1000
		self.learning_rate = 0.00000001

	# This function defines the success metric of the optimizer.
	def evaluate(self, parameters):
		return self.metric([p.evaluate() for p in parameters])

	def metric(self, parameters):
		return 0

	def optimize(self):
		for i in range( self.iterations ):
			print "Iteration %d..." % i
			# Define a normalized weights vector.
			weights = [ random.random() - 0.5 for r in range( len(self.parameters) ) ]
			weights = numpy.divide( weights , math.sqrt(sum( [ w**2 for w in weights ] )))
			# Now let's find the gradient of the metric over this vector.
			left 	= self.evaluate( [ p - w for p,w in zip(self.parameters,weights) ] )
			right	= self.evaluate( [ p + w for p,w in zip(self.parameters,weights) ] )
			print "Current performance: %f " % right
			derivative = right - left
			print "Current derivative: %f " % derivative
			print "Current parameter value: %f" % self.parameters[0]
			# Perform the parameter update.
			self.parameters = [ (p + w*self.learning_rate*derivative) for p,w in zip(self.parameters, weights) ]