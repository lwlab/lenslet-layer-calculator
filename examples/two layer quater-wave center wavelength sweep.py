# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating.permittivity= GenericDataSource(3)

		coating2 			= Layer()
		coating2.name		= "AR coating"
		coating2.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating2.permittivity= GenericDataSource(5)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, silicon]

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

def metric(parameters):
	# The parameters are imported in order.
	depth1 			= parameters[0]
	depth2 			= parameters[1]
	# Set the depth of the coating layer.
	a.structure[1].thickness = GenericDataSource(depth1)
	a.structure[2].thickness = GenericDataSource(depth2)

	# Now we'll loop through the bandwidth and determine
	# the quality of the reflections. Currently using 
	frequencies 	= numpy.union1d( numpy.union1d( numpy.linspace(75,115,5), numpy.linspace(130,170,5) ), numpy.linspace(190,240,5))
	RMS_reflection 	= 0
	for f in frequencies:
		incident_field.frequency = f * 1e9 # f gigahertz
		a.calculate( incident_field )
		RMS_reflection += (a.reflection()*100)**2

	# The higher the metric function, the better the performance. So we'll
	# return a negative number for the RMS to minimize reflections.
	return -numpy.sqrt( RMS_reflection / len(frequencies) )


frequencies 	= numpy.linspace(75,240, 100)
# Set the structure thickness to the optimized thickness.

performance = []

for f in frequencies:
	t1 = 0.25 * 3e8 / (f*1e9 * numpy.sqrt(3))
	t2 = 0.25 * 3e8 / (f*1e9 * numpy.sqrt(5))

	performance.append( metric([ t1, t2 ]) )
	

plot.plot(frequencies, performance,'r-')

#plot.ylim([0,100])
plot.ylabel("Performance [RMS reflection, %]")
plot.xlim([min(frequencies), max(frequencies)])
plot.xlabel("Frequency (GHz)")
plot.show()