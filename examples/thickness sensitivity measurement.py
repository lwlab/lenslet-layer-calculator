# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating.permittivity= GenericDataSource(2)

		coating2 			= Layer()
		coating2.name		= "AR coating"
		coating2.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating2.permittivity= GenericDataSource(4)

		coating3 			= Layer()
		coating3.name		= "AR coating"
		coating3.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating3.permittivity= GenericDataSource(7)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]


a = ARController()
print "Preparing to analyze network:"
print a.visualize()

frequencies 	= numpy.linspace(75,240, 400)
# Set the structure thickness to the optimized thickness.

transmissions = []
control_trans = []
midway_trans  = []

def metric(parameters):
	# The parameters are imported in order.
	depth1 			= parameters[0]
	depth2 			= parameters[1]
	depth3 			= parameters[2]
	# Set the depth of the coating layer.
	a.structure[1].thickness = GenericDataSource(depth1)
	a.structure[2].thickness = GenericDataSource(depth2)
	a.structure[3].thickness = GenericDataSource(depth3)

	# Now we'll loop through the bandwidth and determine
	# the quality of the reflections.
	frequencies 	= [95, 150, 220] # numpy.linspace(75,240, 20)
	average_transmission 	= 0
	for f in frequencies:
		incident_field.frequency = f * 1e9 # f gigahertz
		a.calculate( incident_field )
		average_transmission += a.transmission()**2

	# The higher the metric function, the better the performance. So we'll
	# return a negative number for the RMS to minimize reflections.
	return ( 100*average_transmission / len(frequencies) )

thickness_error = numpy.linspace(0,5,100)
performances 	= []

samples = 1000
errors 	= numpy.linspace(0, 20, 40)
results = []

for e in errors:

	# Now draw samples from the gaussian distribution and evaluate
	# the metric. The figure of 25 micron accuracy is derived from
	# Toki's thesis.

	thickness1 = GaussianDataSource( 0.3533e-3, e/100 * 0.3533e-3 )
	thickness2 = GaussianDataSource( 0.2498e-3, e/100 * 0.2498e-3 )
	thickness3 = GaussianDataSource( 0.1889e-3, e/100 * 0.1889e-3 )

	total = 0
	for i in range(samples):
		total += metric([thickness1.sample(), thickness2.sample(), thickness3.sample()])

	results.append( total / samples )
	print total / samples

plot.plot(errors, results,'b-')

plot.ylim([93,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(errors), max(errors)])
plot.xlabel("Error in thickness [%]")
plot.show()