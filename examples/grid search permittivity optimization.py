# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating.permittivity= GenericDataSource(3)

		coating2 			= Layer()
		coating2.name		= "AR coating"
		coating2.thickness 	= GenericDataSource(75.13e-9) # 75 nm
		coating2.permittivity= GenericDataSource(5)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, silicon]

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

def metric(parameters):
	# The parameters are imported in order.
	e1 			= parameters[0]
	e2			= parameters[1]
	# Set the depth of the coating layer.
	a.structure[1].thickness = GenericDataSource(0.25 * 3e8 / ( 160e9 * numpy.sqrt(e1)))
	a.structure[2].thickness = GenericDataSource(0.25 * 3e8 / ( 160e9 * numpy.sqrt(e2)))
	a.structure[1].permittivity = GenericDataSource(e1)
	a.structure[2].permittivity = GenericDataSource(e2)

	# Now we'll loop through the bandwidth and determine
	# the quality of the reflections. Currently using 
	frequencies 	= numpy.union1d( numpy.union1d( numpy.linspace(75,115,5), numpy.linspace(130,170,5) ), numpy.linspace(190,240,5))
	RMS_reflection 	= 0
	for f in frequencies:
		incident_field.frequency = f * 1e9 # f gigahertz
		a.calculate( incident_field )
		RMS_reflection += (a.reflection()*100)**2

	# The higher the metric function, the better the performance. So we'll
	# return a negative number for the RMS to minimize reflections.
	return -numpy.sqrt( RMS_reflection / len(frequencies) )

# Grid-search over the space of possible thicknesses.
best = -100
bestp = [ 0, 0 ]
iteration = 0

e1domain = numpy.linspace(2, 7.5, 50)
e2domain = numpy.linspace(2, 7.5, 50)

for e1 in e1domain:
	iteration += 1
	print "Currently %0.2f%% done." % (100*iteration/len(e1domain))
	for e2 in e2domain:
		c = metric([e1, e2])
		if c > best:
			best = c
			bestp = [e1, e2]
			print "Found best performer: %f%% RMS @ %s" % (-c, bestp) 

print "Gridsearch complete. Best performance: %f%% RMS. Best parameters: %s" % (best, bestp)
print bestp[0]
print bestp[1]