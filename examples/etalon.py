# Lenslet layer calculator
# by Colin Merkel

# Etalon analysis. Refer to http://scitation.aip.org/content/aip/journal/jap/63/11/10.1063/1.341153
# where the comparable measurements were made on actual Silicon.

from analyzer import *
import matplotlib.pyplot as plot

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class EtalonController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		atmosphere 				= SourceLayer()
		atmosphere.name 		= "Atmosphere"
		atmosphere.thickness	= GenericDataSource(5e-3)
		atmosphere.permittivity	= GenericDataSource(1.00)

		silicon 			= Layer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(13.49e-3) # 13.49 mm
		silicon.permittivity= GenericDataSource(11.6)
		silicon.resistivity = GenericDataSource(5) # 500 ohm cm

		air		 			= TerminationLayer()
		air.name 			= "Atmosphere"
		air.thickness		= GenericDataSource(5e-3)
		air.permittivity	= GenericDataSource(1.00)

		# Hook everything up together.
		atmosphere.right 	= silicon
		silicon.left 		= atmosphere
		silicon.right 		= air
		air.left 	 		= silicon

		# Return everything together.
		self.structure 	= [atmosphere, silicon, air]
		self.source 	= atmosphere
		self.terminator = air

a = EtalonController()
print "Preparing to analyze network:"
print a.visualize()

a.calculate( incident_field )

frequencies = numpy.linspace(25,40, 100)
transmissions = []

for f in frequencies:
	incident_field.frequency = f*1e9 # f gigahertz
	a.calculate( incident_field )
	transmissions.append( a.transmission()*100 )

plot.plot(frequencies, transmissions)
plot.ylim([0,100])
plot.ylabel("Transmission [%]")
plot.xlim([25, 40])
plot.xlabel("Frequency [GHz]")
plot.show()