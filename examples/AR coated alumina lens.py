# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		atmosphere 				= SourceLayer()
		atmosphere.name 		= "Atmosphere"
		atmosphere.thickness	= GenericDataSource(0)
		atmosphere.permittivity	= GenericDataSource(1.00)

		stycast1 			= Layer()
		stycast1.name		= "Stycast #1"
		stycast1.thickness	= GenericDataSource(0.425e-3) # 0.425 mm
		stycast1.permittivity= GenericDataSource(2)

		stycast2 			= Layer()
		stycast2.name		= "Stycast #2"
		stycast2.thickness	= GenericDataSource(0.275e-3) # 0.425 mm
		stycast2.permittivity= GenericDataSource(5)

		alumina 			= Layer()
		alumina.name		= "Alumina"
		alumina.thickness 	= GenericDataSource(50e-3) # 50 mm
		alumina.permittivity= GenericDataSource(10)
		alumina.losstangent = GenericDataSource(1e-3) # 150 ohm meters

		stycast3 			= Layer()
		stycast3.name		= "Stycast #1"
		stycast3.thickness	= GenericDataSource(0.275e-3) # 0.425 mm
		stycast3.permittivity= GenericDataSource(5)

		stycast4 			= Layer()
		stycast4.name		= "Stycast #2"
		stycast4.thickness	= GenericDataSource(0.425e-3) # 0.425 mm
		stycast4.permittivity= GenericDataSource(2)

		vacuum	 			= TerminationLayer()
		vacuum.name 		= "Vaccuum"
		vacuum.thickness	= GenericDataSource(1)
		vacuum.permittivity	= GenericDataSource(1.00)

		# Return everything together.
		self.structure 	= [atmosphere, stycast1, stycast2, alumina, stycast3, stycast4, vacuum]
		self.source 	= atmosphere
		self.terminator = vacuum

a = ARController()
print "Preparing to analyze network:"
print a.visualize()

a.calculate( incident_field )

frequencies 	= numpy.linspace(25, 200, 200)
losstangents 	= [ 1e-5, 5e-5, 1e-4, 5e-4, 1e-3 ]

for t in losstangents:
	transmissions 	= []
	reflectivities 	= []
	a.structure[3].losstangent = GenericDataSource(t)
	print "Calculating for loss tangent %f..." % (t)

	for f in frequencies:
		incident_field.frequency = f*1e9 # f gigahertz
		a.calculate( incident_field )
		transmissions.append( (a.transmission())*100 )


	plot.plot(frequencies, transmissions)

plot.ylim([0,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(frequencies), max(frequencies)])
plot.xlabel("Frequency [GHz]")
plot.show()