# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class TARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating 1"
		coating.thickness 	= GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(2)), 10e-6) # 75 nm
		coating.permittivity= GaussianFixedDataSource(2, 0.1)

		coating2 			= Layer()
		coating2.name		= "AR coating 2"
		coating2.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(3.983)), 10e-6) # 75 nm
		coating2.permittivity= GaussianFixedDataSource(3.983, 0.1)

		coating3 			= Layer()
		coating3.name		= "AR coating 3"
		coating3.thickness 	=  GaussianFixedDataSource(0.25*3e8/(160e9 * numpy.sqrt(7.5)), 10e-6) # 75 nm
		coating3.permittivity= GaussianFixedDataSource(7.5, 0.1)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]

a = TARController()

print "Preparing to analyze network: 'three-layer'"
print a.visualize()

frequencies = numpy.linspace(75,240,20)
curves = []
for c in range(50):
	curves.append( [] )

three_layer_reflection 	= []

for f in frequencies:
	incident_field.frequency = f*1e9
	a.reset()
	a.calculate( incident_field )

	three_layer_reflection.append( b.transmission() )

	e2 = []
	e3 = []
	for c in curves:
		a.scramble()
		a.calculate(incident_field)
		c.append( a.transmission() )

plot.plot(frequencies, two_layer_reflection,   'r-')

for curve in curves: 
	plot.plot(frequencies, curve, 'r-', alpha=0.3)

plot.ylim([0, 1])
plot.ylabel("Transmission [%]")
plot.xlim([75, 240])
plot.xlabel("Frequency [GHz]")
plot.show()