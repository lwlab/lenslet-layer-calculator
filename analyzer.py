# Lenslet layer calculator
# by Colin Merkel

from infrastructure import *
import numpy

class AnalyzerController(object):
	def __init__(self):
		# Define the structure, source and terminator
		self.structure 	= []
		self.source 	= None
		self.terminator = None
		self.starting_power = 0
		# Set up the geometry.
		self.geometry()
		# Connect up all of the pointers within the layers.
		self.interconnect()

	def interconnect(self):
		# The source is always the first element.
		self.source = self.structure[0]
		# The terminator is always the last element.
		self.terminator = self.structure[-1]
		# Now, we need to link together all of the layers so 
		# they know who is left and right of them.
		self.structure[0].right = self.structure[1]
		for i in range(1, len(self.structure) - 1):
			print "Connecting %s..." % self.structure[i].name
			self.structure[i].left = self.structure[i-1]
			self.structure[i].right = self.structure[i+1]

	def scramble(self):
		for l in self.structure:
			l.scramble()

	def reset(self):
		for l in self.structure:
			l.reset()

	def geometry(self): 
		pass

	def visualize(self):
		return self.source.visualize()

	# This function returns the transmission percentage.
	def transmission(self):
		reflected 	= self.source.absorbed_power()
		transmitted = self.terminator.absorbed_power()
		return transmitted / ( self.starting_power )

	# This function returns the reflection percentage.
	def reflection(self):
		reflected 	= self.source.absorbed_power()
		transmitted = self.terminator.absorbed_power()
		return reflected / ( self.starting_power )

	def calculate(self, wave):
		# Clear out any old data that might exist.
		self.terminator.clear()
		self.source.clear()
		# Calibrate all layers for the given frequency.
		for l in self.structure:
			l.calibrate( wave.frequency )
		
		# Calculate the input power.
		self.starting_power = self.source.power( wave.electric_field )
		# Calculate the result by inserting the wave in the
		# left side.
		self.source.receiveLeft( wave )
		
		return True