# lenslet-layer-calculator

This is a program written to use analytical equations derived for one-dimensional plane wave reflections in order to numerically simulate the reflection and transmission coefficients of layered structures. 

### Configuration ###

Edit the file under /config/config.json to specify the parameters for the simulation. All units must be in meters, kilograms and seconds. The only two parameters are:

1. e_field_threshold: the amount (in percent) of the original incoming electric field which can be forgivably thrown away during a computational cycle.
2. coherence_length: the coherence length of the light coming in. If this is set too small, then even large objects will have unrealistic Fabry-Perot fringes. If set too long, then interference effects are not visible. It takes some fiddling around to figure out what the best choice is, but I think it should depend mostly on the frequency resolution of the system (refer to equations relating the coherence length to the frequency resolution of the system).

### How to use this software ###

The codebase consists of two main files: `infrastructure.py` and `analyzer.py`, plus `main.py`, which is used to configure an actual simulation. In order to get started, edit main.py. 

*First*, subclass the AnalyzerController and define a "geometry()" function which specifies	`self.structure`, which is an array of Layer objects. Refer to the example functions to see how this is done.

Then, create an instance of your new subclass. Define an incident field (an instance of Wave) and define the incident electric field and frequency.

Finally, run

	analyzercontroller.calculate( incident_field )

where the analyzercontroller is your subclass instance, and the incident field is the Wave instance that you just defined. This will perform all of the calculations, but it doesn't return anything. Once calculation is complete, you can run something like:

	analyzercontroller.transmission()

which returns the transmission percentage, or 

	analyzercontroller.reflection()

which returns the transmission percentage.