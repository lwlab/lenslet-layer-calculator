# Lenslet layer calculator
# by Colin Merkel

# Antireflection-coated alumina lens from Toki's thesis, page 21.

from analyzer import *
from optimizer import *
import matplotlib.pyplot as plot
import cmath

# Set up the incident field.
incident_field 				= Wave()
incident_field.electric_field = 100    # 100 units of electric field.

class ARController(AnalyzerController):
	def geometry(self):
		self.structure = []
		# In this test, we'll have [ Atmosphere ] -> [ Silicon ].
		vacuum 				= SourceLayer()
		vacuum.name 		= "Vacuum"
		vacuum.thickness	= GenericDataSource(0)
		vacuum.permittivity= GenericDataSource(1.00)

		coating 			= Layer()
		coating.name		= "AR coating"
		coating.thickness 	= GenericDataSource(0.3533e-3) # 75 nm
		coating.permittivity= GenericDataSource(2)

		coating2 			= Layer()
		coating2.name		= "AR coating"
		coating2.thickness 	= GenericDataSource(0.2498e-3) # 75 nm
		coating2.permittivity= GenericDataSource(4)

		coating3 			= Layer()
		coating3.name		= "AR coating"
		coating3.thickness 	= GenericDataSource(0.1889e-3) # 75 nm
		coating3.permittivity= GenericDataSource(7)

		silicon 			= TerminationLayer()
		silicon.name		= "Silicon"
		silicon.thickness 	= GenericDataSource(40e-3) # 4 cm
		silicon.permittivity= GenericDataSource(3.417**2)

		# Return everything together.
		self.structure 	= [vacuum, coating, coating2, coating3, silicon]


a = ARController()
print "Preparing to analyze network:"
print a.visualize()

frequencies 	= numpy.linspace(75,240, 400)
# Set the structure thickness to the optimized thickness.

transmissions = []
control_trans = []
midway_trans  = []

def metric(parameters):
	# The parameters are imported in order.
	p1 			= parameters[0]
	p2 			= parameters[1]
	p3 			= parameters[2]
	frequency 	= parameters[3]
	# Set the depth of the coating layer.
	a.structure[1].permittivity = GenericDataSource(p1)
	a.structure[2].permittivity = GenericDataSource(p2)
	a.structure[3].permittivity = GenericDataSource(p3)

	# Now we'll loop through the bandwidth and determine
	# the quality of the reflections.
	frequencies 	= [frequency] # numpy.linspace(75,240, 20)
	average_transmission 	= 0
	for f in frequencies:
		incident_field.frequency = f * 1e9 # f gigahertz
		a.calculate( incident_field )
		average_transmission += a.transmission()**2

	# The higher the metric function, the better the performance. So we'll
	# return a negative number for the RMS to minimize reflections.
	return ( 100*average_transmission / len(frequencies) )

thickness_error = numpy.linspace(0,5,100)
performances 	= []

samples = 1000
errors 	= numpy.linspace(0, 20, 20)
results_95  = []
results_150 = []
results_220 = []

for e in errors:

	# Now draw samples from the gaussian distribution and evaluate
	# the metric. The figure of 25 micron accuracy is derived from
	# Toki's thesis.

	p1 = GaussianDataSource( 2, e/100 * 2 )
	p2 = GaussianDataSource( 4, e/100 * 4 )
	p3 = GaussianDataSource( 7, e/100 * 7 )

	total_95 = 0
	total_150 = 0
	total_220 = 0
	for i in range(samples):
		total_95 += metric([p1.sample(), p2.sample(), p3.sample(), 95])
		total_150 += metric([p1.sample(), p2.sample(), p3.sample(), 150])
		total_220 += metric([p1.sample(), p2.sample(), p3.sample(), 220])

	results_95.append( total_95 / samples )
	results_150.append( total_150 / samples )
	results_220.append( total_220 / samples )

plot.plot(errors, results_95,'b-')
plot.plot(errors, results_150,'r-')
plot.plot(errors, results_220,'g-')

plot.ylim([92,100])
plot.ylabel("Transmission [%]")
plot.xlim([min(errors), max(errors)])
plot.xlabel("Error in permittivity [%]")
plot.show()

